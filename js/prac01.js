document.getElementById("searchBtn").addEventListener("click", function () {
    const userIdInput = document.getElementById("userId").value;
    const userId = parseInt(userIdInput);

    if (!isNaN(userId)) {
        axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)
            .then(response => {
                const user = response.data;
                updateUserInfo(user);
            })
            .catch(error => {
                console.error('Error al obtener los datos:', error);
                alert('Usuario no encontrado. Por favor, ingrese un ID válido.');
            });
    } else {
        alert('Por favor, ingrese un ID válido.');
    }
});

function updateUserInfo(user) {
    document.getElementById("userName").value = user.name;
    document.getElementById("userUsername").value = user.username;
    document.getElementById("userEmail").value = user.email;
    document.getElementById("userAddress").value = `${user.address.street}`;
    document.getElementById("userAddress2").value = `${user.address.suite}`;
    document.getElementById("userAddress3").value = `${user.address.city}`;

    document.getElementById("userInfoContainer").style.display = "block";
}
