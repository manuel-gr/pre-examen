document.getElementById('btnBuscar').addEventListener('click', function() {
    var nombrePais = document.getElementById('nombrePais').value;
    if (nombrePais.trim() !== '') {
        // Hacer la solicitud a la API con el nombre del país
        fetch(`https://restcountries.com/v3.1/name/${nombrePais}`)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al obtener datos del país');
                }
            })
            .then(data => {
                if (data.length > 0) {
                    displayCountryData(data[0]);
                } else {
                    displayErrorMessage();
                }
            })
            .catch(error => {
                if (error.message.includes('NetworkError')) {
                    console.error('Error de red. Verifica tu conexión a Internet.');
                } else if (error.message.includes('TypeError')) {
                    alert('Error al procesar la respuesta de la API. Asegúrate de que los países estén escritos en inglés.');
                } else {
                    console.error(error.message);
                }
            });
    } else {
        alert('Por favor, ingresa el nombre de un país.');
    }
});

document.getElementById('btnLimpiar').addEventListener('click', function() {
    document.getElementById('countryName').innerText = '';
    document.getElementById('capital').innerText = '';
    document.getElementById('language').innerText = '';
    document.getElementById('bandera').src = '';
    document.getElementById('bandera').alt = '';
});

function displayCountryData(country) {
    document.getElementById('countryName').innerText = country.name.common;
    document.getElementById('capital').innerText = country.capital[0];
    document.getElementById('language').innerText = Object.values(country.languages)[0];

    var banderaElement = document.getElementById('bandera');
    banderaElement.src = country.flags.png || 'https://via.placeholder.com/150';
    banderaElement.alt = `Bandera de ${country.name.common}`;
}

function displayErrorMessage() {
    document.getElementById('resultados').innerHTML = '<p>No se encontraron datos para este país.</p>';
}